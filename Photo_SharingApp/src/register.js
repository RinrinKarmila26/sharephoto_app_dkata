import React, {Component} from 'react';
import {View, TextInput, TouchableOpacity, Image, Button} from 'react-native';
import {styleLogin} from './style';

export default class regist extends Component {
  constructor(props){
    super(props);
    this.state={
      regis:{ 
          userName: '',
          email: '',
          password:'',
          confirmPassword:'',
      }
    }
  }

  Submit = async()=>{
    try {
    await Axios.post('http://20.4.12.225:2034/PhotoSharing', {
        userName: this.state.regis.userName,
        email: this.state.user.email,
        password: this.state.user.password
    })
    }catch(error){
        console.log(error);
    }
}

          
  klikButton = () => {
   // const  ValReg= `/^\([A-Za-z0-9+\@dkatalis.com)`;
     
    const { userName,password, email,confirmPassword } = this.state;
    if(email === "" && email.match(/@rin.com\b/gi)){
      alert(
        "email good"
      )
    }
    else if(password && confirmPassword) {
        alert("Passwords don't match");
        return
      
    } else {
      alert("succes regis")
        return 
    }
}
  
static navigationOptions = {
    title: 'Register',
    headerStyle: {
      backgroundColor: '#73C6B6',
    },
  };


  render() {
    const  {password,confirmPassword,userName,email}= this.state;
    const Invalid =  password !== confirmPassword && password === "" && email === "" ;
    return (
      <View style={styleLogin.container}>
        <View style={styleLogin.AttempContainer}>
          <View style={styleLogin.containerImg}>
            <Image style={styleLogin.img} source={require('./img/icon.png')} />
          </View>

          <View style={styleLogin.ViewInput}>
            <TextInput
              style={styleLogin.in}
              value={this.state.userName}
              onChangeText={userName => this.setState({userName})}
              placeholder={'username'}
            />
          </View>
          <View style={styleLogin.ViewInput}>
            <TextInput
              style={styleLogin.in}
              value={this.state.email}
              onChangeText={email => this.setState({email})}
              
              placeholder={'email'}
            />
          </View>
          <View style={styleLogin.ViewInput}>
            <TextInput
              secureTextEntry={true}
              style={styleLogin.in}
              value={this.state.password}
              //onChangeText={password => this.setState({password})}
              onChangeText={password => this.setState({password})}
              placeholder={'password'}
            />
          </View>
          <View style={styleLogin.ViewInput}>
            <TextInput
              style={styleLogin.in}
               value={this.state.ConfirmPassword}
              onChangeText={ConfirmPassword => this.setState({ConfirmPassword})}
              placeholder={'Confirm password'}
            />
          </View>
          <Button
            title={'register'}
            style={styleLogin.in}
            onPress ={this.klikButton}
          />
        </View>
      </View>
    );
  }
}
