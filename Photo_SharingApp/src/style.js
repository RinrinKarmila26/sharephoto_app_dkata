import {StyleSheet} from 'react-native';

const styleLogin = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'pink',
  },
  in: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 2,
    marginBottom: 10,
  },
  ViewInput: {
   // backgroundColor: 'yellow',
    alignItems: 'center',
    justifyContent: 'center',
   // borderBottomWidth: 3,
    borderBottomColor: 'grey',
  },
  AttempContainer: {
    borderColor: 'black',
    borderWidth: 3,
    borderStyle: 'solid',
    borderRadius: 20,
    width: 350,
    height: 470,
  },
  containerImg: {
    backgroundColor: 'white',
    borderWidth: 4,
    justifyContent: 'center',
    borderColor: 'black',
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  img: {
    position:'relative',
    flex:1,
    justifyContent:'center',
    width: 100,
    height: 90,
  },
});
module.exports = {styleLogin};
