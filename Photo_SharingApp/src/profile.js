import React, {Component} from 'react';
import {View, TextInput, TouchableOpacity, Image, Button} from 'react-native';
import {styleLogin} from './style';
import ImagePicker from 'react-native-image-picker'

export default class profiles extends Component {
      state={
        photo:null
      };
      handleChoose=()=>{
        const options={
          data:true,
        };
        ImagePicker.launchImageLibrary(options, response =>{
          if(response.uri){
            this.setState({photo:response});
          }
        })
      };


    render(){
      const {photo}=this.state;
        return(
        <View>
         <View style={styleLogin.AttempContainer}>
          <View style={styleLogin.containerImg}>
            <Image style={styleLogin.img} source={require('./img/icon.png')} />
          </View>
          <View style={styleLogin.ViewInput}>
            <TextInput
              style={styleLogin.in}
              value={this.state.firstName}
             // onChangeText={email => this.setState({email})}
              
              placeholder={'firstName'}
            />
          </View>
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'} }>
            {photo &&(
              <Image
              source={{uri:photo.uri}}
              style={{width: 150, height: 150}}
              />
            )}
          </View>
          
        <Button title="Choose Photo" onPress={this.handleChoosePhoto} />

        </View>
        </View>

        )
    }
        
}