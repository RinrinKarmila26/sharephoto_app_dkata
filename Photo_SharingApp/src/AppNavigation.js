import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Login from './LoginForm';
import Regist from './register';

const RootStack = createStackNavigator(
  {
    // The Routes
    Home: {screen: Login},
    register: {screen: Regist},
  },
  {
    // Default Route
    initialRouteName: 'Home',
  },
);

// Create app container
const Container = createAppContainer(RootStack);
export default Container;
