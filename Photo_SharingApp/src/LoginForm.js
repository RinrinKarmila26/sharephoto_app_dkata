import React, {Component} from 'react';
import {
  View,
  TextInput,
  TouchableOpacity,
  Text,
  Button,
  Image,
} from 'react-native';
import styleLogin from './style';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      usernameErr:'',
      passwordErr:'',
    };
  }
  
  static navigationOptions = {
    title: 'Login',
    headerStyle: {backgroundColor: '#03A9F4'},
    headerTintColor: '#fff',
    headerTitleStyle: {fontWeight: 'bold'},
  };
  
  render() {
    const navigations = this.props.navigation;
    return (
      <View style={styleLogin.container}>
        <View style={styleLogin.AttempContainer}>
          <View style={styleLogin.containerImg}>
            <Image style={styleLogin.img} source={require('./img/icon.png')} />
          </View>

          <View style={styleLogin.ViewInput}>
          <TextInput
            style={styleLogin.in}
            // value={this.state.username}
            //onChangeText={username => this.setState({username})}
            placeholder={'Username'}
          />
        </View>
        <View style={styleLogin.ViewInput}>
          <TextInput
            style={styleLogin.in}
            // value={this.state.password}
            // onChangeText={password => this.setState({password})}
            placeholder={'password'}
          />
        </View>
        <Button
          title={'Login'}
          style={styleLogin.in}
          // onPress={this.onLogin.bind(this)}
        />
        <TouchableOpacity
          onPress={() => navigations.navigate('register')}>
          <Text> register</Text>
        </TouchableOpacity>
        </View>

       
      </View>
    );
  }
}
