/**
 * @format
 */

import {AppRegistry} from 'react-native';
//import App from './App';
//import Login from './src/LoginForm'
//import regist from './src/register'
import profiles from './src/profile'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => profiles);
