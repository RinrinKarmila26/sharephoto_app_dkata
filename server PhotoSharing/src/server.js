const Hapi = require('@hapi/hapi')
const Qs = require('qs');
const path = require('path');
const { Getuser,addUser} = require('./handler')

// connectMongodb()

const SharingApp = async () => {
    const server = Hapi.Server({
        port : 2034,
        host: '0.0.0.0',//'localhost',//
        query: {
            parser: (query) => Qs.parse(query)
        },

    });
    await server.register(require('@hapi/basic'));
   
    server.route({
        method: 'GET',
        path: '/',
        handler: Getuser,

    });

    server.route({
        method: 'POST',
        path: '/',
        handler: addUser,

    });
    //server.route(routess)

    server.start();
    console.log('server run at : ', server.info.uri)
    
    process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);

});
return server;
}

SharingApp()

