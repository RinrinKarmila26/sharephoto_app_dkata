const mongoose = require('mongoose'); 
const Schema = mongoose.Schema;
try{
    mongoose.connect('mongodb://localhost:27017/PhotoSharing', {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
          })
          
    mongoose.set('debug',true)
    }catch (err){
          console.log(err.message)
          process.exit(1)
    }


const SchemaNew = new Schema({
    id: Number,
    userId: String,
    email: String,
    password : String,
    name: String,
    created_Date : { type: Date, default: Date.now }
   }   
  
)
//buat model
userdb = mongoose.model('users',SchemaNew);

module.exports ={userdb}